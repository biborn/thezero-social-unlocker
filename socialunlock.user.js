// ==UserScript==
// @name          Social Unlocker
// @namespace     http://thezero.org
// @description   Unlock and get rid of that Social Locker
// @include       *
// @grant         none
// ==/UserScript==

// version 1.0
// 2015-07-26
// UnLicense [http://unlicense.org/]

// Supported
//   OnePress Social Locker:
//      http://demo.byonepress.com/sl-wp/example/use-cases/
//      http://demo.byonepress.com/free/social-locker-for-wordpress/
//   Social Traffic Pop:
//      http://codecanyon.net/item/social-traffic-pop-for-wordpress/full_screen_preview/309705
//   Google Traffic Pop Demo:
//      http://codecanyon.net/item/google-traffic-pop/full_screen_preview/299533
//   WP Like Lockout:
//      http://wplikelockout.com/


// TODO:
// Like Locker - http://tyler.tc/devpress/wordpress-like-locker-pro/
// Google+1 Content Locker - http://codecanyon.net/item/google-1-content-locker/full_screen_preview/305388

var s=".onp-sl-secrets,.onp-sl-overlap-locker-box,.onp-sl-social-buttons-enabled,.onp-sl-overlap-background,.onp-sl-overlap-box,.onp-sl-transparence-mode{display:none !important}"+
    ".onp-sl-content{display:block !important}"+
    ".onp-sl-blur-area {filter:none !important}"+
    "#stp-bg,#stp-main{display:none !important}"+
    "#gpp-bg,#gpluspop{display:none !important}"+
    "#rciwpllbg,#rciwpllpop{display:none !important}";

var style = document.createElement("style");
var t = document.createTextNode(s);
style.appendChild(t);
head = document.getElementsByTagName ("head")[0] || document.documentElement;
head.insertBefore(style, head.firstChild);
